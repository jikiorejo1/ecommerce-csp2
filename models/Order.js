// [SECTION] DEPENDENCIES AND MODULES 
let mongoose = require('mongoose');
// [SECTION] BLUEPRINT SCHEMA
let orderSchema = new mongoose.Schema({
    totalAmount:{
        type:Number  
    },
    purchasedOn:{
        type:Date,
        default: new Date()
    },
    orderBy : {
        type: String,
        required:[true,'User who ordered is required ']
    },
    products:[{
        productID:{
        type:String,
        required:[true, 'ProductId is required']
        },
        productQuantity:{
            type:Number,
            required:[true,'product quantity is required']
        }
    },]
    

})

// [SECTION] MODULE EXPORT
let Order =  mongoose.model('Order',orderSchema);
module.exports = Order;