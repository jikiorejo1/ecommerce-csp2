//[SECTION] Packages and Dependencies
  const express = require("express"); 
  const mongoose = require("mongoose");  
  const dotenv = require("dotenv");
  //acquire the routing components for the users collection
  const productRoutes = require('./routes/products'); 
  const orderRoutes = require('./routes/orders');
  const userRoutes = require('./routes/users'); 

//[SECTION] Server Setup
  const app = express(); 
  dotenv.config(); 
  app.use(express.json());
  const secret = process.env.CONNECTION_STRING;
  const port = process.env.PORT || 4000; 

//[SECTION] Application Routes
  app.use('/products', productRoutes);
  app.use('/orders', orderRoutes);
  app.use('/users', userRoutes); 

//[SECTION] Database Connection
  mongoose.connect(secret)
  let connectStatus = mongoose.connection; 
  connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
  app.get('/', (req, res) => {
     res.send(`Capstone 2 Gumana ka na`); 
  }); 
  app.listen(port, () => console.log(`Server is running on port ${port}`)); 
   
