//[SECTION] Dependencies and Modules
const Product = require('../models/Product');

//[SECTION] Functionality [CREATE]
module.exports.createProduct = (info) => {
    let cName = info.name;
    let cDesc = info.description;
    let cCost = info.price;
    let newProduct = new Product({
        name: cName,
        description: cDesc,
        price: cCost
    }) 
       return newProduct.save().then((savedProduct, error) => {
           if (error) {
               return false;
           } else {
               return savedProduct; 
           }
       });
  };

//[SECTION] Functionality [RETRIEVE]
  module.exports.getAllProducts = () => {
    return Product.find({}).then(outcomeNiFind => {
       return outcomeNiFind;
    });
  };

  module.exports.getProduct = (id) => {
     return Product.findById(id).then(resultOfQuery => {
       return resultOfQuery; 
     });
  };

  module.exports.getAllActiveProduct = () => {
     return Product.find({isActive: true}).then(resultOftheQuery => {
         return resultOftheQuery;
     });
  };

//[SECTION] Functionality [UPDATE]
  module.exports.updateProduct = (id, details) => {
     let cName = details.name; 
     let cDesc = details.description;
     let cCost = details.price;   
     let updatedProduct = {
       name: cName,
       description: cDesc,
       price: cCost
     }
     return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
         if (err) {
           return 'Failed to update Product'; 
         } else {
           return 'Successfully Updated Product'; 
         }
     })
  }

  module.exports.deactivateProduct = (id) => {
    let updates = {
      isActive: false
    } 
    return Product.findByIdAndUpdate(id, updates).then((archived, error) => {
       if (archived) {
         return `The Product ${id} has been deactivated`;
       } else {
         return 'Failed to archive Product'; 
       };
    });
  };
 

//[SECTION] Functionality [DELETE]
 module.exports.deleteProduct = (id) => {
   return Product.findByIdAndRemove(id).then((removedProduct, err) => {
       if (err) {
         return 'No Product Was Removed'; 
       } else { 
         return 'Product Succesfully Deleted'; 
       };
   });
 };