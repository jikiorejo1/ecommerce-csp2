// [SECTION] Depencies and Modules
let bcrypt = require("bcrypt");
let User = require("../models/User");
const jwt = require("jsonwebtoken");
const dotenv = require('dotenv').config();
const JWT_KEY = process.env.JWT_KEY;



// fn[authentication]
module.exports.login = (data) => {
  let userEmail = data.email;
  let userPass = data.password;

  return User.findOne({ email: userEmail }).then((foundExistingUser) => {
    if (foundExistingUser) {
      let userHashedPassword = foundExistingUser.password;
      return bcrypt
        .compare(userPass, userHashedPassword)
        .then((isMatch, err) => {
          if (err) {
            return err;
          } else if (!isMatch) {
            return `Wrong Credentials , Please try again`;
          } else {
            
            return this.createToken(foundExistingUser);
          }
        });
    } else {
      return `Wrong Credentials , Please try again`;
    }
  });
};

// [SECTION] TOKEN CREATION
module.exports.createToken = (user) =>{
let userData = {
  email:user.email,
  isAdmin:user.isAdmin,
  id: user._id
}
return jwt.sign(userData ,JWT_KEY,{});
}

// [SECTION] ENSURING TOKEN

module.exports.verify = (req,res,next) =>{
  let token = req.headers.authorization;
  if (typeof token === "undefined"){
      return res.send('Forbidden')
  }else{
      token = token.split(" ");
      let accessToken =  token[1];
      jwt.verify(accessToken, JWT_KEY,(error,decodedToken) =>{
          if (error){
              return res.send('forbidden');
          }else{
              req.user = decodedToken;
              next();
          }
      })
      return 
  }

}


// [SECTION] ENSURE TOKEN IF ADMIN

module.exports.verifyAdmin = (req,res,next) =>{
  if(req.user.isAdmin === true ){
      next();
  }else{
      return res.send('forbidden');
  }
}