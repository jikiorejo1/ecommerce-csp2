/*
	We will create our own module which will have methods that will help us authorize our users to access or to disallow access to certain parts/ features in our app
*/
// imports
const jwt = require("jsonwebtoken");
const secret = "Ecommerce";



/*
	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us authorize our users to access or disallow access to certain parts of our app.

	JWT is like a gift wrapping serving able to encode our user details which can only be unwrapped by jwt's own methods and if the secret provided is intact.

	IF the jwt seemed tampered with we will reject the users attempt to access a feature in our app.

*/



//Token Creation
/*
	- Analogy
		Pack the gift and provide a lock with the secret code as the key
*/
module.exports.createAccessToken = (user) => {
	//check if we can receive the details of the foundUser in our login:
	console.log(user)

	//data object is created to contain some details of our user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method
	// it generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})

};


/*
============================================================================
	Notes on JWT:

	1. You can only get a unique jwt with our secret if you log in to our app with the correct email and password.

	2. As a user, You can only get your own details from your own token from logging in.

	3. JWT is not meant to store sensitive data. For now, for ease of use and for our MVP, we add the email and isAdmin details of the logged in user, however, in the future, you can limit this to only the id and for every route and feature, you can simply lookup for the user in the database to get his details.

	4. JWT is like a more secure passport you use around the app to access certain features meant for your type of user.

	5. We will verify the legitimacy of a JWT every time a user access a restricted feature. Each JWT contains a secret only our server knows. IF the jwt has been, in any way, changed, We will reject the user and his tampered token. IF the jwt, does not contain a secret OR the secret is different, we will reject his access and token.
============================================================================
*/



//Token Verification
/*- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/
module.exports.verify = (req, res, next) => {
	//middlewares which have access to req,res and next also can send responses to the client.

	// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token
	console.log(req.headers.authorization);

	//req.headers.authorization contains sensitive data and especially our token
	let token = req.headers.authorization;


	//IF we are not passing token in our request authorization or in our postman authorization, then here in our api, req.headers.authorization will be undefined.

	//This if statement will first check IF token variable contains undefined or a proper jwt. If it is undefined, we will check token's data type with typeof, then send a message to the client.
	if(typeof token === "undefined"){

		return res.send({auth: "Failed. No Token"});
	
	} else {

		console.log(token);
		/*
			slice() is a method which can be used on strings and arrays.

			This will allow us to copy a part of the string.

			slice(<startingPosition>,<endPosition>)

			Starting position indicates the index number we will copy from.

			End position indicates the index number we will copy up to but not including the item in that position.
			
			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZjBjMzYyNmNhYzJjM2VhYTBmY2I5YSIsImVtYWlsIjoic3BpZGVybWFuM0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjQzMjQ1MTEyfQ.c29qelk9GkrnZP10M6wqo6fiTKHPk-c15DcpSBsKq7I 

			"Peter"
			
			slice(3,string.length)

			"er"

			Essentially, we extracted just the jwt token and re-assigned to our token variable.

		*/
		token = token.slice(7, token.length);

		console.log(token);

		// Validate the token using the "verify" method decrypting the token using the secret code
		jwt.verify(token, secret, function(err, decodedToken){
			//err will contain the error from decoding your token. This will contain the reason why we will reject the token.
			//IF verification of the token is a success, then jwt.verify will return the decoded token.
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				});

			} else {

				console.log(decodedToken);//contains the data from our token
				
				req.user = decodedToken
				//user property will be added to request object and will contain our decodedToken. It can be accessed in the next middleware/controller.

				next();
				//middleware function
				//next() will let us proceed to the next middleware OR controller
			}
		})
	}
};




//verifyAdmin will also be used a middleware.
module.exports.verifyAdmin = (req, res, next) => {
	//verifyAdmin comes after the verify middleware,
	//Do we have any access to our user's isAdmin detail?
	//We can get details from req.user because verifyAdmin comes after verify method.
	//Note: You can only have req.user for any middleware or controller that comes after verify.

	//console.log(req.user);
	if(req.user.isAdmin){
		//if the logged in user, based on his token is an admin, we will proceed to the next middleware/controller
		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}