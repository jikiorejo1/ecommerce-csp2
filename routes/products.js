//[SECTION] Dependencies and Modules
  const exp = require("express");
  const controller = require('../controllers/products');

  //We will import auth module so we can use our verify and verifyAdmin method as middleware for our routes.
  const auth = require("../auth") 

//destructure verify and verifyAdmin from auth. Auth, is an imported module, so therefore it is an object in JS.
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 

//[SECTION] [POST] Routes
route.post('/create', verify, verifyAdmin, (req, res) => { 
  let data = req.body; 
  controller.createProduct(data).then(outcome => {
       res.send(outcome); 
  });
});

//[SECTION] [GET] Routes
route.get('/all', verify, verifyAdmin, (req, res) => {
 controller.getAllProducts().then(outcome => {
   res.send(outcome);
 });
});

route.get('/:id', (req, res) => {
 let productId = req.params.id; 
 controller.getProduct(productId).then(result => { 
     res.send(result);
 }); 
}); 

route.get('/', (req, res) => {
 controller.getAllActiveProduct().then(outcome => {
    res.send(outcome); 
 });
});

//[SECTION] [PUT] Routes
route.put('/:id', verify, verifyAdmin, (req, res) => {
 let id = req.params.id; 
 let details = req.body;
 let cName = details.name; 
 let cDesc = details.description;
 let cCost = details.price;       
 if (cName  !== '' && cDesc !== '' && cCost !== '') {
   controller.updateProduct(id, details).then(outcome => {
       res.send(outcome); 
   });      
 } else {
   res.send('Incorrect Input, Make sure details are complete');
 }
}); 

route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
 let productId = req.params.id; 
 controller.deactivateProduct(productId).then(resultOfTheFunction => {
     res.send(resultOfTheFunction);
 });
});

//[SECTION] [DEL] Routes 
route.delete('/:id', verify, verifyAdmin, (req, res) => {
let id = req.params.id; 
controller.deleteProduct(id).then(outcome => {
   res.send(outcome);
});
});

//[SECTION] Export Route System
  module.exports = route; 
